{
  description = "A flake for SGDBoop";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = {
    self,
    nixpkgs,
  }: let
    supportedSystems = [
      "x86_64-linux"
    ];
    forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);
  in {
    packages = forAllSystems (system: let
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
    in {
      cd = pkgs.callPackage ./cd.nix {};
      im = pkgs.callPackage ./im.nix {};
      iup = pkgs.callPackage ./iup.nix {
        cd = self.packages.${system}.cd;
        im = self.packages.${system}.im;
      };
      sgdboop = pkgs.callPackage ./default.nix {iup = self.packages.${system}.iup;};
      default = self.packages.${system}.sgdboop;
    });
  };
}
