# SGDBoop-Flake

A Nix flake for [SGDBoop](https://github.com/SteamGridDB/SGDBoop)

# Credit

All credit for the flake goes to [puffnfresh](https://github.com/puffnfresh) for the [original PR](https://github.com/NixOS/nixpkgs/pull/269369). Sadly that PR seemingly went stale so in the meant time this flake exist

